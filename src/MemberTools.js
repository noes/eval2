class MemberTools {
	constructor(setter, caller, value) {
		this.setter = setter;
		this.caller = caller;
		this.value = value;
	}
}

module.exports = MemberTools;