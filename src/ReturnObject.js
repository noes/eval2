class ReturnObject {
	shouldReturn = false;
	returnValue = undefined;
	shouldBreak = false;
	shouldContinue = false;
}

module.exports = ReturnObject;