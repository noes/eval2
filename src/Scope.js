const cons = require('./constants.js');

class Scope {
	stack = undefined;

	constructor(stack = []) {
		this.stack = stack;
	}

	addLevel = (scopeLevel) => {
		if (typeof scopeLevel !== 'object' || scopeLevel === null) {
			throw TypeError('addLevel requires an object as an argument.');
		}
		this.stack.push(scopeLevel);
	}

	getLevelWithVariable = (name) => {
		for (let i = this.stack.length - 1; i >= 0; i--) {
			const level = this.stack[i];
			if (name in level) {
				return level;
			}
		}
		if (name in globalThis) {
			return globalThis;
		}
		return undefined;
	}

	getCurrentLevel = () => {
		if (this.stack.length === 0) {
			return undefined;
		}
		return this.stack[this.stack.length - 1];
	}

	getValue = (name) => {
		const level = this.getLevelWithVariable(name);
		if (level === undefined) {
			throw new ReferenceError(`Attempt to access undefined variable/property: "${name}"`);
		}
		return level[name];
	}

	createVariable = (name, value) => {
		if (name in cons.reserved) {
			throw new SyntaxError(`${name} is a reserved word.`);
		}
		const current = this.getCurrentLevel();
		if (!current) {
			throw new ReferenceError(`There is no current scope level`);
		}
		if (name in current) {
			throw new ReferenceError(`There is already a variable named '${name}'`);
		} else {
			current[name] = value;
		}
	}

	updateVariable = (name, value) => {
		if (name in cons.reserved) {
			throw new SyntaxError(`${name} is a reserved word.`);
		}
		
		const level = this.getLevelWithVariable(name);
		if (!level) {
			throw new ReferenceError(`Can't update undeclared variable ${name}`);
		}

		level[name] = value;

	}

	copy = () => {
		return new Scope([...this.stack]);
	}
}

module.exports = Scope;