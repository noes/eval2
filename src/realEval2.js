const cons = require('./constants.js');
const Scope = require('./Scope.js');
const ReturnObject = require('./ReturnObject.js');
const MemberTools = require('./MemberTools.js');

// this takes an arrowExpression
const realEval2 = (arrowExpression, scope) => {

	const body = arrowExpression.body;
	if (body.type !== cons.statement.BLOCK) {
		throw new SyntaxError('Arrow functions must use block statement for body')
	}

	const blockStatement = body.body;

	return (...rest) => {

		// duplicate scope and add a level
		let newScope = scope.copy();
		newScope.addLevel({});

		// assign method arguments to new local scope level
		if (arrowExpression.params) {

			const params = arrowExpression.params;

			for (let i = 0; i < params.length; i++) {

				const param = params[i];
				let value;

				if (rest.length >= i) {
					value = rest[i];
				} else {
					value = undefined;
				}

				newScope.createVariable(param.name, value);

			}

		}

		const executioner = new Executioner();
		const returnObject = new ReturnObject();

		executioner.execute(blockStatement, returnObject, newScope);

		return returnObject.value;
	};

}

module.exports = realEval2;


class Executioner {

	// used to execute all the code in a block
	execute = (block, returnObject, scope = new Scope()) => {

		for (let i = 0; i < block.length; i++) {

			if (returnObject.shouldReturn) {
				return;
			} else if (returnObject.shouldContinue) {
				return;
			} else if (returnObject.shouldBreak) {
				return;
			}

			let code = block[i];

			this.processCode(code, returnObject, scope);

		}

	}

	processCode = (code, returnObject, scope) => {

		switch (code.type) {
			case cons.declaration.VAR: {
				this.processDeclaration(code, scope);
				break;
			}
			case cons.declaration.FUNC: {
				throw new SyntaxError('Only arrow functions allowed.');
			}
			case cons.statement.TRY: {
				this.processTry(code, returnObject, scope);
				break;
			}
			case cons.statement.IF: {
				this.processIf(code, returnObject, scope);
				break;
			}
			case cons.statement.WHILE: {
				this.processWhile(code, returnObject, scope);
				break;
			}
			case cons.statement.FOR: {
				this.processFor(code, returnObject, scope);
				break;
			}
			case cons.statement.EXPRESSION: {
				this.processExpression(code.expression, scope);
				break;
			}
			case cons.statement.BREAK: {
				returnObject.shouldBreak = true;
				break;
			}
			case cons.statement.CONTINUE: {
				returnObject.shouldContinue = true;
				break;
			}
			case cons.statement.RETURN: {
				returnObject.shouldReturn = true;
				if (code.argument == null) {
					returnObject.value = undefined;
				} else {
					returnObject.value = this.processExpression(code.argument, scope);
				}
				break;
			}
		}
	}

	processDeclaration = (code, scope) => {

		if (code.kind !== 'let') {
			throw new SyntaxError('Only "let" is allowed when declaring variables.');
		}
		if (code.declarations.length > 1) {
			throw new SyntaxError('Only one declaration at a time');
		}
		const dec = code.declarations[0];
		const name = dec.id.name;
		const init = dec.init;

		let value = this.processExpression(init, scope);

		scope.createVariable(name, value);
	}

	processTry = (code, returnObject, scope) => {

		const shouldReturn = () => {
			return returnObject.shouldBreak || returnObject.shouldContinue
			|| returnObject.shouldReturn
		}

		try {

			let newScope = scope.copy();
			newScope.addLevel({});
			this.execute(code.block.body, returnObject, newScope);

			if (shouldReturn()) {
				return;
			}

		} catch (e) {

			let newScope = scope.copy();
			newScope.addLevel({})
			newScope.createVariable(code.handler.param.name, e);

			this.execute(code.handler.body.body, returnObject, newScope);

			if (shouldReturn()) {
				return;
			}

		} finally {

			if (!code.finalizer) {
				return;
			}

			let newScope = scope.copy();
			newScope.addLevel({})

			this.execute(code.finalizer.body, returnObject, newScope);

			if (shouldReturn()) {
				return;
			}

		}
	}
	
	processIf = (code, returnObject, scope) => {

		if (this.processExpression(code.test, scope)) {
			
			if (code.consequent.type !== cons.statement.BLOCK) {

				throw SyntaxError('If/else if/else statements require a block');

			} else {
				const newScope = scope.copy();
				newScope.addLevel({});
				this.execute(code.consequent.body, returnObject, newScope);
			}

		} else if (code.alternate?.type === cons.statement.BLOCK) {

			const newScope = scope.copy();
			newScope.addLevel({});
			this.execute(code.alternate.body, returnObject, newScope);

		} else if (code.alternate?.type === cons.statement.IF) {

			this.processIf(code.alternate, returnObject, scope);

		}
	}

	processWhile = (expression, returnObject, scope) => {
		if (expression.body.type !== cons.statement.BLOCK) {
			throw SyntaxError('Body of while loop must be a block');
		}

		while (this.processExpression(expression.test, scope)) {
			const newScope = scope.copy();
			newScope.addLevel({});
			this.execute(expression.body.body, returnObject, newScope);
			if (returnObject.shouldBreak) {
				returnObject.shouldBreak = false;
				break;
			} else if (returnObject.shouldContinue) {
				returnObject.shouldContinue = false;
				continue;
			} else if (returnObject.shouldReturn) {
				return;
			}
		}
	}
	
	processFor = (expression, returnObject, scope) => {

		if (expression.body.type !== cons.statement.BLOCK) {
			throw SyntaxError('Body of for loop must be a block');
		}

		const initScope = scope.copy();
		initScope.addLevel({});
		
		this.processDeclaration(expression.init, initScope);

		while (this.processExpression(expression.test, initScope)) {

			const newScope = initScope.copy();
			newScope.addLevel({});
			this.execute(expression.body.body, returnObject, newScope);
			if (returnObject.shouldBreak) {
				returnObject.shouldBreak = false;
				break;
			} else if (returnObject.shouldContinue) {
				returnObject.shouldContinue = false;
			} else if (returnObject.shouldReturn) {
				return;
			}
			this.processExpression(expression.update, initScope);

		}
	}

	processExpression = (expression, scope) => {

		switch (expression.type) {
			case cons.expression.IDENTIFIER: {
				return scope.getValue(expression.name);
			}
			case cons.expression.LITERAL: {
				return expression.value;
			}
			case cons.expression.TEMPLATE: {
				return this.processTemplate(expression, scope);
			}
			case cons.expression.ASSIGNMENT: {
				return this.processAssignment(expression, scope);
			}
			case cons.expression.UNARY: {
				return this.processUnary(expression, scope);
			}
			case cons.expression.UPDATE: {
				return this.processUpdate(expression, scope);
			}
			case cons.expression.ARROW: {
				return realEval2(expression, scope);
			}
			case cons.expression.CALL: {
				return this.processCall(expression, scope);
			}
			case cons.expression.BINARY: {
				return this.processBinary(expression, scope);
			}
			case cons.expression.LOGICAL: {
				return this.processLogical(expression, scope);
			}
			case cons.expression.NEW: {
				return this.processNew(expression, scope);
			}
			case cons.expression.OBJECT: {
				return this.processObject(expression, scope);
			}
			case cons.expression.ARRAY: {
				return this.processArray(expression, scope);
			}
			case cons.expression.MEMBER: {
				return this.getMemberTools(expression, scope).value;
			}
		}
	}


	processAssignment = (expression, scope) => {

		const operator = expression.operator;
		const left = expression.left;
		const right = expression.right;
		let assignee = undefined;
		let value = undefined;

		switch (left.type) {
			case cons.expression.IDENTIFIER: {
				assignee = left.name;
				break;
			}
			case cons.expression.MEMBER: {
				// assignee will be a method in this case
				assignee = this.getMemberTools(left, scope).setter;
				break;
			}
		}

		value = this.processExpression(right, scope);

		if (typeof assignee === 'function') {

			assignee(value, operator);

		} else {

			const oldValue = scope.getValue(assignee);

			if (operator === '=') {
				value = value; 
			} else if (operator === '+=') {
				value = oldValue + value;
			} else if (operator === '-=') {
				value = oldValue - value;
			} else if (operator === '*=') {
				value = oldValue * value;
			} else if (operator === '/=') {
				value = oldValue / value;
			}

			scope.updateVariable(assignee, value);
		}

		return value;
	}

	processUnary = (expression, scope) => {
		const operator = expression.operator;
		let value = this.processExpression(expression.argument, scope);

		switch (operator) {
			case '-': {
				return -value;
			}
			case '+': {
				return +value;
			}
			case '!': {
				return !value;
			}
			case 'typeof': {
				return typeof value;
			}
		}
	}

	processUpdate = (expression, scope) => {
		const change = expression.operator == '++' ? 1 : -1;
		const prefix = expression.prefix;

		let value;
		let setter;

		if (expression.argument.type === cons.expression.IDENTIFIER) {
			value = this.processExpression(expression.argument, scope);
			setter = (newValue) => scope.updateVariable(expression.argument.name, newValue);
		} else if (expression.argument.type === cons.expression.MEMBER) {
			const tools = this.getMemberTools(expression.argument, scope);
			value = tools.value;
			setter = (newValue) => tools.setter(newValue, '=');
		} else {
			throw SyntaxError('Only Identifiers and Member properties can be updated');
		}

		let oldValue = value;
		let newValue = value + change;

		setter(newValue);

		if (prefix) {
			return newValue;
		} else {
			return oldValue;
		}
	}

	processTemplate = (expression, scope) => {
		const quasis = expression.quasis;
		const expressions = expression.expressions;

		const combination = [];
		
		quasis.forEach((q, i) => {
			combination.push(q.value.raw);
			if (i < expressions.length) {
				combination.push(this.processExpression(expressions[i], scope))
			}
		});

		return combination.join('');
	}

	processBinary = (expression, scope) => {
		const operator = expression.operator;

		let leftVal = this.processExpression(expression.left, scope);
		let rightVal = this.processExpression(expression.right, scope);

		switch (operator) {
			case '+': {
				return leftVal + rightVal;
			}
			case '-': {
				return leftVal - rightVal;
			}
			case '*': {
				return leftVal * rightVal;
			}
			case '%': {
				return leftVal % rightVal;
			}
			case '/': {
				return leftVal / rightVal;
			}
			case '==': {
				return leftVal == rightVal;
			}
			case '===': {
				return leftVal === rightVal;
			}
			case '!=': {
				return leftVal != rightVal;
			}
			case '1==': {
				return leftVal !== rightVal;
			}
			case '<': {
				return leftVal < rightVal;
			}
			case '<=': {
				return leftVal <= rightVal;
			}
			case '>': {
				return leftVal > rightVal;
			}
			case '>=': {
				return leftVal === rightVal;
			}
		}
	}

	processLogical = (expression, scope) => {
		const operator = expression.operator;

		let leftVal = this.processExpression(expression.left, scope);
		let rightVal = this.processExpression(expression.right, scope);

		switch (operator) {
			case '&&': {
				return leftVal && rightVal;
			}
			case '||': {
				return leftVal || rightVal;
			}
		}
	}

	processCall = (expression, scope) => {
		let args = expression.arguments.map((arg) => {
			return this.processExpression(arg, scope);
		});

		if (expression.callee.type === cons.expression.MEMBER) {
			return this.getMemberTools(expression.callee, scope).caller(args);
		} else {
			let callee = this.processExpression(expression.callee, scope);
			return callee.apply(null, args);
		}
	}

	processNew = (expression, scope) => {
		const args = expression.arguments.map((arg) => {
			return this.processExpression(arg, scope);
		});

		const Class = this.processExpression(expression.callee, scope);

		// we can't use apply or call with 'new' statements in some cases,
		// and the behaviour of some constructors depends on the numbers
		// of arguments, so we have to handle each case
		switch (args.length) {
			case 0: {
				return new Class();
			}
			case 1: {
				return new Class(args[0]);
			}
			case 2: {
				return new Class(args[0], args[1]);
			}
			case 3: {
				return new Class(args[0], args[1], args[2]);
			}
			case 4: {
				return new Class(args[0], args[1], args[2], args[3]);
			}
			case 5: {
				return new Class(args[0], args[1], args[2], args[3], args[4]);
			}
			case 6: {
				return new Class(args[0], args[1], args[2], args[3], args[4], args[5]);
			}
			case 7: {
				return new Class(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
			}
			case 8: {
				return new Class(
					args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]
				);
			}
			case 9: {
				return new Class(
					args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]
				);
			}
			case 10: {
				return new Class(
					args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8],
					args[9]
				);
			}
			case 11: {
				return new Class(
					args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8],
					args[9], args[10]
				);
			}
			case 12: {
				return new Class(
					args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8],
					args[9], args[10], args[11]
				);
			}
		}

	}

	processObject = (expression, scope) => {
		let object = {};
		expression.properties.forEach((prop) => {
			let key;
			if (prop.key.type === cons.expression.IDENTIFIER && !prop.computed) {
				// this is a case like { b: 1}; 
				// Without this check it would look for a variable called 'b'
				key = prop.key.name;
			} else {
				this.processExpression(prop.key, scope);
			}
			const value = this.processExpression(prop.value, scope);
			object[key] = value;
		});

		return object;
	}

	processArray = (expression, scope) => {
		return expression.elements.map((el) => {
			return this.processExpression(el);
		});
	}

	getMemberTools = (expression, scope) => {

		const property = expression.property;
		let parent = this.processExpression(expression.object, scope);

		if (parent instanceof MemberTools) {
			parent = parent.value;
		}

		let name = 'name' in property ? property.name : property.value;

		const setter = (newValue, operator) => {
			if (operator === '=') {
				parent[name] = newValue; 
			} else if (operator === '+=') {
				parent[name] = parent[name] + newValue;
			} else if (operator === '-=') {
				parent[name] = parent[name] - newValue;
			} else if (operator === '*=') {
				parent[name] = parent[name] * newValue;
			} else if (operator === '/=') {
				parent[name] = parent[name] / newValue;
			}
			return parent[name];
		};

		const value = parent[name];

		const caller = (args) => {
			return value.apply(parent, args);
		}

		return new MemberTools(setter, caller, value);

	}
}