const Scope = require('../Scope.js');

describe('Scope', () => {
	let scope;

	beforeEach(() => {
		scope = new Scope();
	});

  test('Scope - creating variable with no level', () => {
		const func = () => {
			scope.createVariable('test', 1);
		}
    expect(func).toThrow(ReferenceError);
	});

	test('Scope - adding non-object level', () => {
		let func = () => {
			scope.addLevel(2);
		}
		expect(func).toThrow(TypeError);
		
		func = () => {
			scope.addLevel(null);
		}
    expect(func).toThrow(TypeError);
	});
	
	test('Scope - creating duplicate variable on same level', () => {
		const func = () => {
			scope.addLevel({});
			scope.createVariable('test', 1);
			scope.createVariable('test', 2);
		}
    expect(func).toThrow(ReferenceError);
	});
	
	test('Scope - creating variable', () => {
		scope.addLevel({});
		scope.createVariable('test', 1);
    expect(scope.getValue('test')).toBe(1);
	});
	
	test('Scope - creating variable with same name on new level', () => {
		scope.addLevel({});
		scope.createVariable('test', 1);
		scope.addLevel({});
		scope.createVariable('test', 2);
    expect(scope.getValue('test')).toBe(2);
	});
	
	test('Scope - updating variable', () => {
		scope.addLevel({});
		scope.createVariable('test', 1);
		scope.updateVariable('test', 2);
    expect(scope.getValue('test')).toBe(2);
	});
	
	test('Scope - Access undefined variable', () => {
		const func = () => {
			scope.addLevel({});
			scope.getValue('undefined variable');
		};
    expect(func).toThrow(ReferenceError);
	});
	
	test('Scope - Access global variable', () => {
		scope.addLevel({});
    expect(scope.getValue('Array')).toBe(Array);
  });

});