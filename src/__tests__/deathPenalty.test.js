const deathPenalty = require('../../index.js');

describe('wrong formatting', () => {

  test('creating non arrow function', () => {
		const func = () => {
			deathPenalty('let a = 2;');
		}
    expect(func).toThrow(SyntaxError);
	});

	test('Arrow function without block statement', () => {
		const func = () => {
			deathPenalty('() => 2');
		}
    expect(func).toThrow(SyntaxError);
	});

	test('more than one top level item', () => {
		let func = () => {
			deathPenalty(`
				() => {};
				() => {};
			`);
		}
		expect(func).toThrow(SyntaxError);
		
		func = () => {
			deathPenalty(`
				() => {};
				let a = 2;
			`);
		}
    expect(func).toThrow(SyntaxError);
	});

});


describe('declaring and returning', () => {

  test('let a = 2', () => {

		const func = deathPenalty(
			`() => { 
				let a = 2; 
				return a;
			}`
		);
		expect(func()).toBe(2);
		
	});

	test('return literal 2', () => {

		const func = deathPenalty(
			`() => { 
				return 2;
			}`
		);
		expect(func()).toBe(2);
		
	});

	test('return literal string', () => {

		const func = deathPenalty(
			`() => { 
				return "asdf";
			}`
		);
		expect(func()).toBe("asdf");
		
	});

	test('return literal boolean', () => {

		const func = deathPenalty(
			`() => { 
				return true;
			}`
		);
		expect(func()).toBe(true);
		
	});

	test('return undefined', () => {

		let func = deathPenalty(
			`() => { 
				return undefined;
			}`
		);
		expect(func()).toBe(undefined);

		func = deathPenalty(
			`() => { 
				return;
			}`
		);
		expect(func()).toBe(undefined);
		
	});


	test('return global', () => {

		let func = deathPenalty(
			`() => { 
				return Array;
			}`
		);
		expect(func()).toBe(Array);
		
	});

});

describe('- assigning', () => {

  test('a = 1', () => {

		const func = deathPenalty(
			`() => { 
				let a = 2;
				a = 1; 
				return a;
			}`
		);
		expect(func()).toBe(1);
		
	});

	test('return and assign', () => {

		const func = deathPenalty(
			`() => { 
				let a = 2; 
				return a = 3;
			}`
		);
		expect(func()).toBe(3);
		
	});

	test('global member assign', () => {

		const func = deathPenalty(
			`() => { 
				globalThis.a = 10;
			}`
		);
		func();
		expect(globalThis.a).toBe(10);
		
	});

	test('global member literal assign', () => {

		const func = deathPenalty(
			`() => { 
				globalThis['a'] = 10;
			}`
		);
		func();
		expect(globalThis.a).toBe(10);
		
	});

	test('global member literal assign unary', () => {

		const func = deathPenalty(
			`() => { 
				globalThis['a'] = -10;
			}`
		);
		func();
		expect(globalThis.a).toBe(-10);
		
	});

	test('global member assign 2', () => {

		globalThis.a = { b: 1};

		const func = deathPenalty(
			`() => { 
				globalThis.a.b = 10;
			}`
		);
		func();
		expect(globalThis.a.b).toBe(10);

		delete globalThis.a;
		
	});

	test('assign from object member', () => {

		globalThis.a = { b: 1 };

		const func = deathPenalty(
			`(val) => { 
				let a = globalThis.a.b;
				return a;
			}`
		);
		expect(func()).toBe(1);

		delete globalThis.a;
		
	});

	test('global member assign with arg', () => {

		globalThis.a = { b: 1 };

		const func = deathPenalty(
			`(val) => { 
				globalThis.a.b = val;
			}`
		);
		func(9);
		expect(globalThis.a.b).toBe(9);

		delete globalThis.a;
		
	});

	test('global member access member of undefined', () => {

		globalThis.a = { b: 1 };

		const func = deathPenalty(
			`() => { 
				globalThis.c.c.a = 9;
			}`
		);

		expect(func).toThrow(TypeError);

		delete globalThis.a;
		
	});

	test('assignment with +=,-=, *=, /=', () => {

		let func = deathPenalty(
			`() => { 
				let a = 2;
				a += 3;
				return a;
			}`
		);
		expect(func()).toBe(5);

		func = deathPenalty(
			`() => { 
				let a = 2;
				a -= 3;
				return a;
			}`
		);
		expect(func()).toBe(-1);

		func = deathPenalty(
			`() => { 
				let a = 2;
				a *= 3;
				return a;
			}`
		);
		expect(func()).toBe(6);

		func = deathPenalty(
			`() => { 
				let a = 2;
				a /= 3;
				return a;
			}`
		);
		expect(func()).toBe(2/3);
		
	});

	test('member assignment with +=,-=, *=, /=', () => {

		globalThis.a = { b: 1 };

		let func = deathPenalty(
			`() => {
				globalThis.a.b += 2;
			}`
		);
		func();
		expect(globalThis.a.b).toBe(3);

		func = deathPenalty(
			`() => {
				globalThis.a.b -= 10;
			}`
		);
		func();
		expect(globalThis.a.b).toBe(-7);

		func = deathPenalty(
			`() => {
				globalThis.a.b *= 2;
			}`
		);
		func();
		expect(globalThis.a.b).toBe(-14);

		func = deathPenalty(
			`() => {
				globalThis.a.b /= -14;
			}`
		);
		func();
		expect(globalThis.a.b).toBe(1);

		delete globalThis.a;
		
	});

});

describe('update expressions', () => {

	test('++ prefix', () => {
		const func = deathPenalty(`() => {
			let a = 2;
			return ++a;
		}`);
    expect(func()).toBe(3);
	});

	test('++ suffix', () => {
		const func = deathPenalty(`() => {
			let a = 2;
			return a++;
		}`);
    expect(func()).toBe(2);
	});

	test('-- prefix', () => {
		const func = deathPenalty(`() => {
			let a = 2;
			return --a
		}`);
    expect(func()).toBe(1);
	});

	test('-- suffix', () => {
		const func = deathPenalty(`() => {
			let a = 2;
			return a--;
		}`);
    expect(func()).toBe(2);
	});

	test('-- array', () => {
		const func = deathPenalty(`() => {
			let a = [1,2,3];
			a[2]--;
			return a;
		}`);
    expect(func()[2]).toBe(2);
	});

	test('object ++', () => {
		const func = deathPenalty(`() => {
			let a = { b: 1}
			a.b++;
			return a;
		}`);
    expect(func().b).toBe(2);
	});

	test('complicated --', () => {
		const func = deathPenalty(`() => {
			let c = { d: 0 }
			let a = { 
				b: () => { return c }
			};
			return 2 * --a.b().d;
		}`);
    expect(func()).toBe(-2);
	});

});

describe('unary expressions', () => {

  test('assign -2', () => {
		const func = deathPenalty(`() => {
			let a = -2;
			return a;
		}`);
    expect(func()).toBe(-2);
	});

	test('create negative member property', () => {
		const func = deathPenalty(`(val) => {
			let a = -val.b;
			return a;
		}`);
    expect(func({ b: 5 })).toBe(-5);
	});

	test('! unary', () => {
		const func = deathPenalty(`() => {
			return !true;
		}`);
    expect(func()).toBe(false);
	});


	test('typeof unary', () => {
		const func = deathPenalty(`() => {
			return typeof true;
		}`);
    expect(func()).toBe('boolean');
	});

});

describe('binary expressions', () => {

  test('2 * 3', () => {
		const func = deathPenalty(`() => {
			let a = 2 * 3;
			return a;
		}`);
    expect(func()).toBe(6);
	});

	test('2 + 3 * 5', () => {
		const func = deathPenalty(`() => {
			let a = 2 + 3 * 5;
			return a;
		}`);
    expect(func()).toBe(17);
	});

	test('2 + 3 / 3', () => {
		const func = deathPenalty(`() => {
			let a = 2 + 3 / 3;
			return a;
		}`);
    expect(func()).toBe(3);
	});

	test('(a = 2) * a', () => {
		const func = deathPenalty(`() => {
			let a = 4;
			let b = (a = 2) * a;
			return b;
		}`);
    expect(func()).toBe(4);
	});

	test('binary with member', () => {
		const func = deathPenalty(`(obj) => {
			let a = 4 * obj.a;
			return a;
		}`);
    expect(func({ a: 2 })).toBe(8);
	});

	test('complex binary', () => {
		const func = deathPenalty(`(obj) => {
			let a = 4 * (obj.a = 2) * obj.a + (8 / 2 + (6 * 3));
			return a;
		}`);
    expect(func({ a: 5 })).toBe(38);
	});

});

describe('logical expressions', () => {

	test('&&', () => {
		const func = deathPenalty(`() => {
			return true && false;
		}`);
    expect(func()).toBe(false);
	});

	test('||', () => {
		const func = deathPenalty(`() => {
			return true || false;
		}`);
    expect(func()).toBe(true);
	});

	test('val || 5', () => {
		const func = deathPenalty(`(val) => {
			return val || 5;
		}`);
    expect(func(false)).toBe(5);
	});

	test('0 && 5', () => {
		const func = deathPenalty(`() => {
			return 0 && 5;
		}`);
    expect(func()).toBe(0);
	});

});


describe('arrow expressions with call expression', () => {

  test('let a = () => { return 2 + 3 }', () => {
		const func = deathPenalty(`() => {
			let a = () => { return 2 + 3 };
			let b = a();
			return b;
		}`);
    expect(func()).toBe(5);
	});

	test('return a function', () => {
		const func = deathPenalty(`() => {
			let a = () => { return 2 + 3 };
			return a;
		}`);
		let a = func();
    expect(a()).toBe(5);
	});

	test('return a function with args', () => {
		const func = deathPenalty(`() => {
			let a = (a, b) => { return a + b };
			return a;
		}`);
		let a = func();
    expect(a(2, 3)).toBe(5);
	});

	test('test mapping', () => {
		const func = deathPenalty(`() => {
			let a = [1,2,3];
			let b = a.map((value) => { return value + 1 });
			return b;
		}`);
		let a = func();
		expect(a).toEqual([2,3,4]);

	});
	

});

describe('call expressions', () => {

  test('push to array', () => {
		const func = deathPenalty(`(arr) => {
			arr.push(3);
		}`);
		let arr = [];
		func(arr);
    expect(arr[0]).toBe(3);
	});

	test('call passed function', () => {
		const func = deathPenalty(`(f) => {
			return f();
		}`);
		let a = func(function() { return 3; })
    expect(a).toBe(3);
	});

	test('complex accesss to call', () => {
		const func = deathPenalty(`(obj) => {
			return obj.a.b[0]();
		}`);
		let a = { a: { b: [() => { return 'hello world' }]}}
		let val = func(a);
    expect(val).toBe('hello world');
	});

	test('chained function calls', () => {
		const func = deathPenalty(`() => {
			let a = '123';
			return a.split('').slice(0, 1);
		}`);
    expect(func()).toEqual(['1']);
	});

});

describe('new expressions', () => {

  test('new array', () => {
		const func = deathPenalty(`() => {
			let a = new Array(3);
			return a;
		}`);
		let arr = func();
    expect(arr.length).toBe(3);
	});

	test('new array with more than one arg', () => {
		const func = deathPenalty(`() => {
			let a = new Array(3, 4, 5);
			return a;
		}`);
		let arr = func();
    expect(arr[2]).toBe(5);
	});

	test('complex access to constructor', () => {
		const func = deathPenalty(`(obj) => {
			let a = new obj.a.b.c[5](3, 4, 5);
			return a;
		}`);
		let arr = func({ a: { b: { c: [1,2,3,4,5, Array]}}});
    expect(arr[2]).toBe(5);
	});

	test('promise', async () => {
		const func = deathPenalty(`() => {
			return new Promise((resolve) => {
				setTimeout(() => {
					resolve(1);
				}, 0);
			})
		}`);
		return func().then((val) => {
			expect(val).toBe(1);
		});
	});


});

describe('object expressions', () => {

  test('simple objects', () => {
		const func = deathPenalty(`() => {
			let a = {
				b: 1
			};
			return a.b;
		}`);
    expect(func()).toBe(1);
	});


  test('with method', () => {
		const func = deathPenalty(`() => {
			let a = {
				b: () => { return 1; }
			};
			return a;
		}`);
    expect(func().b()).toBe(1);
	});

	test('nested assignment', () => {
		const func = deathPenalty(`() => {
			let a = {
				b: {
					c: () => { return a; }
				}
			};
			a.b.c = 2;
			return a;
		}`);
		let a = func();
    expect(a.b.c).toBe(2);
	});

	test('return object from function', () => {
		const func = deathPenalty(`() => {
			let a = () => {
				return {
					value: 2,
				};
			}
			return a;
		}`);
		let returnedFunc = func();
    expect(returnedFunc().value).toBe(2);
	});

	test('object in array', () => {
		const func = deathPenalty(`() => {
			let a = [{ b: 1}];
			return a;
		}`);
    expect(func()[0].b).toBe(1);
	});

});

describe('template literals', () => {

  test('simple teplate, no interpollation', () => {
		const func = deathPenalty(`() => {
			let a = \`asdf\`;
			return a
		}`);
    expect(func()).toBe('asdf');
	});

	test('teplate with interpollation', () => {
		let func = deathPenalty(`() => {
			let a = 2;
			let b = \`as\${a}df\`;
			return b;
		}`);
		expect(func()).toBe('as2df');
		
		func = deathPenalty(`() => {
			let a = 2;
			let b = \`as\${a}\`;
			return b;
		}`);
		expect(func()).toBe('as2');
		
		func = deathPenalty(`() => {
			let a = 2;
			let b = \`\${a}as\`;
			return b;
		}`);
    expect(func()).toBe('2as');
	});

});


describe('arrays', () => {

  test('simple initialization', () => {
		const func = deathPenalty(`() => {
			let a = [];
			return a;
		}`);
		expect(Array.isArray(func())).toBe(true);
	});

	test('complex initialization', () => {
		const func = deathPenalty(`() => {
			let a = [1,2,3];
			a.push(4);
			return a;
		}`);
		let arr = func();
		expect(arr.length).toBe(4);
		expect(arr[2]).toBe(3);
		expect(arr[3]).toBe(4);
	});

	test('nested arrays and access', () => {
		const func = deathPenalty(`() => {
			let a = [[],[],[[],[5]]];
			return a[2][1][0];
		}`);
		expect(func()).toBe(5);
	});

});

describe('if statements', () => {

	test('if true', () => {
		const func = deathPenalty(`() => {
			let a = 5;
			if (true) {
				a = 6;
			}
			return a;
		}`);
		expect(func()).toBe(6);
	});

	test('if false', () => {
		const func = deathPenalty(`() => {
			let a = 5;
			if (false) {
				a = 6;
			}
			return a;
		}`);
		expect(func()).toBe(5);
	});

	test('if else if ', () => {
		const func = deathPenalty(`() => {
			let a = true;
			let b = false;
			let c = 5;
			if (!a) {
				c = 6;
			} else if (!b) {
				c = 7;
			}
			return c;
		}`);
		expect(func()).toBe(7);
	});

	test('if - else if - else ', () => {
		const func = deathPenalty(`() => {
			let a = 5;
			if (false) {
				a = 6;
			} else if (false) {
				a = 7;
			} else {
				a = 8;
			}
			return a;
		}`);
		expect(func()).toBe(8);
	});

	test('return from if', () => {
		const func = deathPenalty(`() => {
			if (true) {
				return true;
			}
			return false;
		}`);
		expect(func()).toBe(true);
	});

	test('return from else', () => {
		const func = deathPenalty(`() => {
			if (false) {
				return 'if';
			} else {
				return 'else';
			}
		}`);
		expect(func()).toBe('else');
	});

	test('scope in if statement', () => {
		const func = deathPenalty(`() => {
			let a = 2;
			if (true) {
				let a = 3;
			}
			return a;
		}`);
		expect(func()).toBe(2);
	});

});

describe('while loops', () => {

	test('while', () => {
		const func = deathPenalty(`() => {
			let a = 1;
			while (a < 10) {
				a = a + 1;
			}
			return a;
		}`);
		expect(func()).toBe(10);
	});

	test('while break', () => {
		const func = deathPenalty(`() => {
			let a = 1;
			while (a < 10) {
				a = a + 1;
				if (a === 5) {
					break;
				}
			}
			return a;
		}`);
		expect(func()).toBe(5);
	});

	test('while continue', () => {
		const func = deathPenalty(`() => {
			let i = 0;
			let a = [];
			while (i < 3) {
				i = i + 1;
				if (i == 2) {
					continue;
				}
				a.push(1);
			}
			return a;
		}`);
		expect(func().length).toBe(2);
	});

});

describe('for loops', () => {

	test('for', () => {
		const func = deathPenalty(`() => {
			let a = 0;
			for (let i = 0; i < 10; i++) {
				a = a + 1;
			}
			return a;
		}`);
		expect(func()).toBe(10);
	});

	test('for loop double skip', () => {
		const func = deathPenalty(`() => {
			let a = 0;
			for (let i = 0; i < 10; i += 3) {
				a = a + 1;
			}
			return a;
		}`);
		expect(func()).toBe(4);
	});

	test('for loop continue and break', () => {
		const func = deathPenalty(`() => {
			let a = 0;
			for (let i = 0; i < 10; i++) {
				break;
				a = a + 1;
			}
			for (let i = 0; i < 10; i++) {
				continue;
				a = a + 1;
			}
			return a;
		}`);
		expect(func()).toBe(0);
	});

});

describe('try catch', () => {

	test('try catch only', () => {

		const func = deathPenalty(`() => {
			let a = 0;
			try {
				window.a.a.a = 2;
			} catch (e) {
				a = 1;
			}
			return a;
		}`);
		expect(func()).toBe(1);
	});

	test('try catch and finally', () => {
		const func = deathPenalty(`() => {
			let a = 0;
			try {
				a = 2;
			} catch (e) {
				a = 1;
			} finally {
				a = 3;
			}
			return a;
		}`);
		expect(func()).toBe(3);
	});

	test('breaking in catch', () => {
		const func = deathPenalty(`() => {

			let a = 0;

			for (let i = 0; i < 10; i++) {

				try {
					a++;
					asdadsf();
				} catch (e) {
					break;
				} finally {

				}

			}

			return a;
		}`);
		expect(func()).toBe(1);
	});

	test('return in catch', () => {
		const func = deathPenalty(`() => {
			let a = 0;
			for (let i = 0; i < 10; i++) {
				try {
					a++;
					asdadsf();
				} catch (e) {
					return 2;
				}
			}
			return a;
		}`);
		expect(func()).toBe(2);
	});

	test('continue in catch', () => {
		const func = deathPenalty(`() => {
			let a = 0;
			for (let i = 0; i < 10; i++) {
				try {
					if (i % 2 == 0) {
						asdadsf();
					}
				} catch (e) {
					continue;
				} finally {
					a++;
				}
			}
			return a;
		}`);
		expect(func()).toBe(5);
	});

});

describe('Big test', () => {

	test('Long complicated test', () => {
		const func = deathPenalty(`
			() => {
				let processWord = (word) => {
					let cutWordInHalf = (word) => {
						let newLength = Math.round(word.length / 2);
						return word.split('').slice(0, newLength).join('');
					}
					word = word + word + word;
					word = word.toLowerCase();
					return cutWordInHalf(word);
				}
				let a = processWord('AAAA');

				return a;

			}`
		);
		expect(func()).toBe('aaaaaa');
	})

});

