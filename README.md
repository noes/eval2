# deathPenalty

## Overview

deathPenalty is meant to perform the same thing as the 'eval' function, but without using eval. So it's basically a simple interpreter for JS written in JS.

## Usage

```
const deathPenalty = require('deathPenalty');

const codeString = `
  () => {
    let a = 2;
	return a;
  }
`;
const func = deathPenalty(codeString);

console.log(func()); // '2'
```
## Purpose

The Chrome team has decided to enhance security in extensions by removing the eval function from Manifest V3.  This was meant to disable extensions from concealing behaviour by loading remotely-hosted code after installation and executing it with eval.

Since I have been making an extension that requires eval, I was curious to know if I could get around it by manually executing code, which led to deathPenalty.

## limitations
deathPenalty can only execute a subset of JavaScript. More features may be added later, but right now several features are not implemented.
Here are a list of limitations:

* Only arrow functions are allowed
* No do-while loops
* No async - await
* No for-in or for-of loops
* No ternary operators
* No optional chaining
* No es6 classes
* No constructor functions (but you can instantiate existing ones with 'new'
* Probably more that I haven't thought of
* All code must be inside an arrow function which will be returned by deathPenatly

Even with these limitations, there is enough left over to do basically anything you want.