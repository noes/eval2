const esprima = require('esprima');
const cons = require('./src/constants.js');

const realEval2 = require('./src/realEval2.js');
const Scope = require('./src/Scope.js');

/**
 * deathPenalty takes a string of JS code and returns
 * a function that executes it without using eval.
 * 
 * @param {String} program     -- js code
 */
const deathPenalty = (code) => {

	const scope = new Scope();
	scope.addLevel({});

	let program;

	try {

		program = esprima.parseScript(code);

	} catch (e) {

		throw e.message;

	}

	if (program.body.length !== 1) {
		throw new SyntaxError('deathPenalty must be passed a single top level arrow function.');
	}

	const expression = program.body[0]?.expression;

	if (expression?.type !== cons.expression.ARROW) {
		throw new SyntaxError('deathPenalty requires top level to be arrow function expression.');
	}

	return realEval2(expression, scope);

}

module.exports = deathPenalty;